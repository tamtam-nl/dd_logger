<?php

namespace Drupal\dd_logger\Client;


use Drupal\dd_logger\Vendor\LeLogger;

/**
 * Class LogEntriesClient
 * @package Drupal\dd_logger\Client
 */
class LogEntriesClient implements ClientInterface
{

    /**
     * @var \Drupal\dd_logger\Client\Credentials
     */
    private $credentials;
    private $vendor;

    /**
     * LogEntriesClient constructor.
     * @param \Drupal\dd_logger\Client\Credentials $credentials
     */
    public function __construct(Credentials $credentials)
    {
        $this->credentials = $credentials;
    }


    /**
     * @param $message
     * @param int $level
     * @return mixed|void
     */
    public function log($message, $level = LOG_INFO)
    {
        // Silently fail if no log ID is provided
        if($this->credentials->getLogId() === null){
            return;
        }

        $this->getLogger()->log($message, $level);
    }

    /**
     * @return \Drupal\dd_logger\Vendor\LeLogger
     */
    private function getLogger(){

        if($this->vendor === null){
            $this->vendor =  LeLogger::getLogger(
                $this->credentials->getLogId(),
                $this->credentials->getPersistent(),
                $this->credentials->getSsl(),
                LOG_DEBUG,
                false,
                '',
                '',
                '',
                $this->credentials->getHostName(),
                false,
                false
            );
        }

        return $this->vendor;
    }
}
