<?php

namespace Drupal\dd_logger\Client;


/**
 * Interface ClientInterface
 * @package Drupal\dd_logger\Client
 */
interface ClientInterface
{

    /**
     * @param $message
     * @param int $level
     * @return mixed
     */
    public function log($message, $level = LOG_INFO);

}