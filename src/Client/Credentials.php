<?php

namespace Drupal\dd_logger\Client;

use Drupal\Core\Site\Settings;

/**
 * Class Credentials
 * @package Drupal\dd_logger\Client
 */
class Credentials
{

    /**
     * @var mixed
     */
    private $logId;

    /**
     * @var mixed
     */
    private $persistent;

    /**
     * @var mixed
     */
    private $ssl;

    /**
     * @var mixed
     */
    private $hostId;

    /**
     * @var mixed
     */
    private $hostName;

    /**
     * Credentials constructor.
     * @param \Drupal\Core\Site\Settings $settings
     */
    public function __construct(Settings $settings)
    {
        $this->logId = $settings->get('dd_logger.log_id', null);
        $this->persistent = $settings->get('dd_logger.persistent', true);
        $this->ssl = $settings->get('dd_logger.ssl', true);
        $this->hostId = $settings->get('dd_logger.host_id', 'dd_logger');
        $this->hostName = $settings->get('dd_logger.persistent', 'dd_logger');
    }

    /**
     * @return mixed
     */
    public function getLogId()
    {
        return $this->logId;
    }

    /**
     * @param mixed $logId
     */
    public function setLogId($logId)
    {
        $this->logId = $logId;
    }

    /**
     * @return mixed
     */
    public function getPersistent(){
       return $this->persistent;
    }

    /**
     * @return mixed
     */
    public function getSsl()
    {
        return $this->ssl;
    }

    /**
     * @param mixed $ssl
     */
    public function setSsl($ssl)
    {
        $this->ssl = $ssl;
    }

    /**
     * @return mixed
     */
    public function getHostId()
    {
        return $this->hostId;
    }

    /**
     * @param mixed $hostId
     */
    public function setHostId($hostId)
    {
        $this->hostId = $hostId;
    }

    /**
     * @return mixed
     */
    public function getHostName()
    {
        return $this->hostName;
    }

    /**
     * @param mixed $hostName
     */
    public function setHostName($hostName)
    {
        $this->hostName = $hostName;
    }

}