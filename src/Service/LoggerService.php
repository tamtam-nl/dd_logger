<?php

namespace Drupal\dd_logger\Service;

use Drupal\Core\Logger\LogMessageParserInterface;
use Drupal\Core\Logger\RfcLoggerTrait;
use Drupal\dd_logger\Client\ClientInterface;
use Psr\Log\LoggerInterface;

/**
 * Class LoggerService
 * @package Drupal\dd_logger\Service
 */
class LoggerService implements LoggerInterface
{
    use RfcLoggerTrait;

    /**
     * @var \Drupal\dd_logger\Client\ClientInterface
     */
    private $client;

    private $parser;

    /**
     * LoggerService constructor.
     * @param \Drupal\dd_logger\Client\ClientInterface $client
     */
    public function __construct(ClientInterface $client, LogMessageParserInterface $parser)
    {
        $this->client = $client;
        $this->parser = $parser;
    }

    /**
     * Logs with an arbitrary level.
     *
     * @param mixed $level
     * @param string $message
     * @param array $context
     *
     * @return void
     */
    public function log($level, $message, array $context = array())
    {
        $variables = $this->parser->parseMessagePlaceholders($logMessage, $context);
        $find = array_keys($variables);
        $replace = array_values($variables);
        $message = str_ireplace($find, $replace, $message);
        $this->client->log($message, $level);
    }
}